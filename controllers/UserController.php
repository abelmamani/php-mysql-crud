<?php
include_once __DIR__ . '/../models/UserModel.php';

class UserController
{
    public static function index(){
        return UserModel::getAllUsers();
    }

    public static function create($data){
        $user = UserModel::getUserByEmail($data['correo']);
        if (!$user){
            UserModel::createUser($data);
            header('Location: /ceroi/views/user/listUser.php');
        }else{
            session_start();
            $_SESSION['register_error'] = 'El usuario ya existe';
            header('Location: /ceroi/views/user/createUser.php');
        }
        exit();
    }

    public static function edit($id)
    {
        return UserModel::getUserById($id);
    }

    public static function update($id, $data)
    {
        UserModel::updateUser($id, $data);
        header('Location: /ceroi/views/user/listUser.php');
        exit();
    }

    public static function delete($id)
    {
        UserModel::deleteUser($id);
        header('Location: /ceroi/views/user/listUser.php');
        exit();
    }
}

// Acciones
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['action'])) {
    $action = $_POST['action'];
    switch ($action) {
        case 'create':
            $data = [
                'nombre' => $_POST['nombre'],
                'correo' => $_POST['correo'],
                'contrasenia' => $_POST['contrasenia']
            ];
            UserController::create($data);
            break;
        case 'update':
            $id = $_POST['id'];
            $data = [
                'nombre' => $_POST['nombre'],
                'correo' => $_POST['correo'],
                'contrasenia' => $_POST['contrasenia']
            ];
            UserController::update($id, $data);
            break;
        }
    }

    if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['action'])) {
        $action = $_GET['action'];
        switch ($action) {
            case 'delete':
                $id = $_GET['id'];
                UserController::delete($id);
                break;
        }
}
?>
