<?php
include_once __DIR__ . '/../models/AlumnoModel.php';

class AlumnoController
{
    public static function index()
    {
        return AlumnoModel::getAllAlumnos();
    }

    public static function create($data)
    {
        AlumnoModel::createAlumno($data);
        header('Location: /ceroi/views/alumno/listaAlumno.php');
        exit();
    }

    public static function edit($id)
    {
        return AlumnoModel::getAlumnoById($id);
    }

    public static function update($id, $data)
    {
        AlumnoModel::updateAlumno($id, $data);
        header('Location: /ceroi/views/alumno/listaAlumno.php');
        exit();
    }

    public static function delete($id)
    {
        AlumnoModel::deleteAlumno($id);
        header('Location: /ceroi/views/alumno/listaAlumno.php');
        exit();
    }
}

// Acciones
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['action'])) {
    $action = $_POST['action'];
    switch ($action) {
        case 'create':
            $data = [
                'nombre' => $_POST['nombre'],
                'apellido' => $_POST['apellido'],
                'dni' => $_POST['dni'],
                'fechaNacimiento' => $_POST['fechaNacimiento']
            ];
            AlumnoController::create($data);
            break;
        case 'update':
            $id = $_POST['id'];
            $data = [
                'nombre' => $_POST['nombre'],
                'apellido' => $_POST['apellido'],
                'dni' => $_POST['dni'],
                'fechaNacimiento' => $_POST['fechaNacimiento']
            ];
            AlumnoController::update($id, $data);
            break;
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['action'])) {
    $action = $_GET['action'];
    switch ($action) {
        case 'delete':
            $id = $_GET['id'];
            AlumnoController::delete($id);
            break;
    }
}
?>
