<?php
include_once __DIR__ . '/../models/UserModel.php';
class AuthController
{
    public static function register($data){
        $user = UserModel::getUserByEmail($data['correo']);
        if (!$user) {
            UserModel::createUser($data);
            AuthController::login($data);
            exit();
        } else {
            session_start();
            $_SESSION['register_error'] = 'El usuario ya existe';
            header('Location: ../views/auth/register.php');
            exit();
        }
    }

    public static function login($data){
        $user = UserModel::getUserByEmailAndPassword($data);

        if ($user) {
            session_start();
            $_SESSION['user_id'] = $user['id'];
            $_SESSION['username'] = $user['correo'];
            header('Location: ../../ceroi/index.php');
            exit();
        } else {
            session_start();
            $_SESSION['login_error'] = 'Credenciales inválidas';
            header('Location: ../views/auth/login.php');
            exit();
        }
    }

    public static function logout(){
        session_start();
        session_destroy();
        header('Location: ../views/auth/login.php');
        exit();
    }
}
// Acciones
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['action'])) {
    $action = $_POST['action'];
    switch ($action) {
        case 'register':
            $data = [
                'nombre' => $_POST['nombre'],
                'correo' => $_POST['correo'],
                'contrasenia' => $_POST['contrasenia']
            ];
            AuthController::register($data);
            break;
        case 'login':
            $data = [
                'correo' => $_POST['correo'],
                'contrasenia' => $_POST['contrasenia']
            ];
            AuthController::login($data);
            break;
        case 'logout':
            AuthController::logout();
             break;
        }
    }
?>