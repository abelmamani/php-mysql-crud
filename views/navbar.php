
<nav class="navbar navbar-expand-lg bg-body-tertiary">
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="/ceroi">Navbar</a>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="<?= BASE_URL ?>">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL ?>views/user/listUser.php">Usuarios</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= BASE_URL ?>views/alumno/listaAlumno.php">Alumnos</a>
        </li>
      </ul>
      <form action="/ceroi/controllers/AuthController.php" method="post">
        <a> <?php echo $_SESSION['username']; ?></a>
        <input type="hidden" name="action" value="logout">
        <button class="btn btn-outline-success" type="submit">Cerrar  Sesion</button>
      </form>
    </div>
  </div>
</nav>