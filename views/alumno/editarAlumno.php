<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Editar Alumno</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
</head>
<body>

<?php
include_once '../../utils/auth.php';
include_once './../../controllers/AlumnoController.php';
$alumno = AlumnoController::edit($_GET['id']);
?>

<div class="container mt-5">
    <h2>Editar Alumno</h2>
    <form action="../../controllers/AlumnoController.php" method="post">
        <input type="hidden" name="action" value="update">
        <input type="hidden" name="id" value="<?= $alumno['id'] ?>">
        
        <div class="mb-3">
            <label for="nombre" class="form-label">Nombre:</label>
            <input type="text" class="form-control" id="nombre" name="nombre" value="<?= $alumno['nombre'] ?>" required>
        </div>
        
        <div class="mb-3">
            <label for="apellido" class="form-label">Apellido:</label>
            <input type="text" class="form-control" id="apellido" name="apellido" value="<?= $alumno['apellido'] ?>" required>
        </div>

        <div class="mb-3">
            <label for="dni" class="form-label">DNI:</label>
            <input type="text" class="form-control" id="dni" name="dni" value="<?= $alumno['dni'] ?>" required>
        </div>

        <div class="mb-3">
            <label for="fechaNacimiento" class="form-label">Fecha de Nacimiento:</label>
            <input type="date" class="form-control" id="fechaNacimiento" name="fechaNacimiento" value="<?= $alumno['fechaNacimiento'] ?>" required>
        </div>
        
        <button type="submit" class="btn btn-primary">Guardar Cambios</button>
        <a href="listaAlumno.php" class="btn btn-secondary">Volver</a>
    </form>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="../js/scripts.js"></script>
</body>
</html>
