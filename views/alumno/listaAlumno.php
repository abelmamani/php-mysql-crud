<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Lista alumnos</title>
    <link rel="stylesheet" href="/ceroi/css/styles.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
</head>
<body>
<?php 
define('BASE_URL', '/ceroi/');
include_once '../../utils/auth.php';
include_once '../navbar.php' ?>
<div class="container mt-5">
    <h2>Alumnos</h2>
    <a href="crearAlumno.php" class="btn btn-primary mb-3">Agregar Alumno</a>
    <?php include_once '../../controllers/AlumnoController.php'; ?>
    <?php $alumnos = AlumnoController::index(); ?>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>DNI</th>
            <th>Fecha de Nacimiento</th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($alumnos as $alumno) : ?>
            <tr>
                <td><?= $alumno['id'] ?></td>
                <td><?= $alumno['nombre'] ?></td>
                <td><?= $alumno['apellido'] ?></td>
                <td><?= $alumno['dni'] ?></td>
                <td><?= $alumno['fechaNacimiento'] ?></td>
                <td>
                    <a href="editarAlumno.php?id=<?= $alumno['id'] ?>" class="btn btn-outline-primary btn-sm">Editar</a>
                    <a href="../../controllers/AlumnoController.php?action=delete&id=<?= $alumno['id'] ?>" class="btn btn-outline-danger btn-sm" onclick="return confirm('¿Estás seguro de eliminar este alumno?')">Eliminar</a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="js/scripts.js"></script>
</body>
</html>
