<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Registrarse</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
</head>
<body>

<div class="container mt-5">
    <?php
    session_start();
    // Verifica si hay un mensaje de error
    if (isset($_SESSION['register_error'])) {
        echo '<div class="alert alert-danger" role="alert">' . $_SESSION['register_error'] . '</div>';
        // Elimina el mensaje de error de la sesión
        unset($_SESSION['register_error']);
    }
    ?>
    <h2>Registrarse</h2>
    <form action="../../controllers/AuthController.php" method="post">
        <input type="hidden" name="action" value="register">
        
        <div class="mb-3">
            <label for="nombre" class="form-label">Nombre:</label>
            <input type="text" class="form-control" id="nombre" name="nombre" required>
        </div>
        
        <div class="mb-3">
            <label for="correo" class="form-label">Correo:</label>
            <input type="email" class="form-control" id="correo" name="correo" required>
        </div>

        <div class="mb-3">
            <label for="contrasenia" class="form-label">Contraseña: </label>
            <input type="text" class="form-control" id="contrasenia" name="contrasenia" required>
        </div>

        <button type="submit" class="btn btn-primary">Registrarme</button>
        <a href="login.php" class="btn btn-secondary">Iniciar Sesision</a>
    </form>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="../js/scripts.js"></script>
</body>
</html>
