<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Iniciar Sesion</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
</head>
<body>

<div class="container mt-5">
    <?php
    session_start();
    // Verifica si hay un mensaje de error
    if (isset($_SESSION['login_error'])) {
        echo '<div class="alert alert-danger" role="alert">' . $_SESSION['login_error'] . '</div>';
        // Elimina el mensaje de error de la sesión
        unset($_SESSION['login_error']);
    }
    ?>
    <h2>Iniciar sesion</h2>
    <form action="../../controllers/AuthController.php" method="post">
        <input type="hidden" name="action" value="login">
        <div class="mb-3">
            <label for="correo" class="form-label">Correo:</label>
            <input type="email" class="form-control" id="correo" name="correo" required>
        </div>

        <div class="mb-3">
            <label for="contrasenia" class="form-label">Contraseña: </label>
            <input type="password" class="form-control" id="contrasenia" name="contrasenia" required>
        </div>

        <button type="submit" class="btn btn-primary">Iniciar Sesion</button>
        <a href="register.php" class="btn btn-secondary">registrarse</a>
    </form>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>

