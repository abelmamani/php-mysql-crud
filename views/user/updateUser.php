<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Editar Usuario</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
</head>
<body>

<?php 
include_once '../../utils/auth.php';
include_once './../../controllers/UserController.php';
$user = UserController::edit($_GET['id']);
?>

<div class="container mt-5">
    <h2>Editar Usuario</h2>
    <form action="../../controllers/UserController.php" method="post">
        <input type="hidden" name="action" value="update">
        <input type="hidden" name="id" value="<?= $user['id'] ?>">
        
        <div class="mb-3">
            <label for="nombre" class="form-label">Nombre:</label>
            <input type="text" class="form-control" id="nombre" name="nombre" value="<?= $user['nombre'] ?>" required>
        </div>
        
        <div class="mb-3">
            <label for="correo" class="form-label">Correo:</label>
            <input type="email" class="form-control" id="correo" name="correo" value="<?= $user['correo'] ?>" required>
        </div>
        
        <div class="mb-3">
            <label for="contrasenia" class="form-label">Contraseña: </label>
            <input type="text" class="form-control" id="contrasenia" name="contrasenia" value="<?= $user['contrasenia'] ?>" required>
        </div>
        
        <button type="submit" class="btn btn-primary">Guardar Cambios</button>
        <a href="listUser.php" class="btn btn-secondary">Volver</a>
    </form>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="../js/scripts.js"></script>
</body>
</html>
