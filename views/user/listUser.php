<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Listar Usuarios</title>
    <link rel="stylesheet" href="/ceroi/css/styles.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css">
</head>
<body>
<?php 
include_once '../../utils/auth.php';
define('BASE_URL', '/ceroi/');
include_once '../navbar.php' ?>
<div class="container mt-5">
    <h2>Usuarios</h2>
    <a href="createUser.php" class="btn btn-primary mb-3">Agregar Usuario</a>
    <?php include_once '../../controllers/UserController.php'; ?>
    <?php $users = UserController::index(); ?>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Correo</th>
            <th>Contraseña/th>
            <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user) : ?>
            <tr>
                <td><?= $user['id'] ?></td>
                <td><?= $user['nombre'] ?></td>
                <td><?= $user['correo'] ?></td>
                <td><?= $user['contrasenia'] ?></td>
                <td>
                    <a href="updateUser.php?id=<?= $user['id'] ?>" class="btn btn-outline-primary btn-sm">Editar</a>
                    <a href="../../controllers/UserController.php?action=delete&id=<?= $user['id'] ?>" class="btn btn-outline-danger btn-sm" onclick="return confirm('¿Estás seguro de eliminar este usuario?')">Eliminar</a>
                 
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>
