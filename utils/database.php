<?php
class Database
{
    private static $host = 'localhost';
    private static $user = 'root';
    private static $password = 'Kepler452b123';
    private static $database = 'ceroi';

    public static function connect()
    {
        $conn = new mysqli(self::$host, self::$user, self::$password, self::$database);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        return $conn;
    }
}
