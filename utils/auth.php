<?php
session_start();
if (!isset($_SESSION['user_id'])) {
    header('Location: /ceroi/views/auth/login.php');
    exit();
}
?>