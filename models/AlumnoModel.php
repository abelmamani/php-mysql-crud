<?php
include_once __DIR__ . '/../utils/database.php';

class AlumnoModel
{
    public static function getAllAlumnos()
    {
        $conn = Database::connect();
        $sql = 'SELECT * FROM alumnos';
        $result = $conn->query($sql);
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public static function getAlumnoById($id)
    {
        $conn = Database::connect();
        $sql = "SELECT * FROM alumnos WHERE id = $id";
        $result = $conn->query($sql);
        return $result->fetch_assoc();
    }

    public static function createAlumno($data)
    {
        $conn = Database::connect();
        $nombre = $data['nombre'];
        $apellido = $data['apellido'];
        $dni = $data['dni'];
        $fechaNacimiento = $data['fechaNacimiento'];

        $sql = "INSERT INTO alumnos (nombre, apellido, dni, fechaNacimiento) VALUES ('$nombre', '$apellido', '$dni', '$fechaNacimiento')";
        $conn->query($sql);
    }

    public static function updateAlumno($id, $data)
    {
        $conn = Database::connect();
        $nombre = $data['nombre'];
        $apellido = $data['apellido'];
        $dni = $data['dni'];
        $fechaNacimiento = $data['fechaNacimiento'];

        $sql = "UPDATE alumnos SET nombre = '$nombre', apellido = '$apellido', dni = '$dni', fechaNacimiento = '$fechaNacimiento' WHERE id = $id";
        $conn->query($sql);
    }

    public static function deleteAlumno($id)
    {
        $conn = Database::connect();
        $sql = "DELETE FROM alumnos WHERE id = $id";
        $conn->query($sql);
    }
}
