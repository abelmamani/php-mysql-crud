<?php
include_once __DIR__ . '/../utils/database.php';

class UserModel
{
    public static function getAllUsers()
    {
        $conn = Database::connect();
        $sql = 'SELECT * FROM usuarios';
        $result = $conn->query($sql);
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public static function getUserById($id){
        $conn = Database::connect();
        $sql = "SELECT * FROM usuarios WHERE id = $id";
        $result = $conn->query($sql);
        return $result->fetch_assoc();
    }

    public static function getUserByEmail($correo){
        $conn = Database::connect();
    
        // Usamos una declaración preparada para evitar la inyección SQL
        $sql = "SELECT * FROM usuarios WHERE correo = ?";
        $stmt = $conn->prepare($sql);
    
        // Vinculamos el parámetro
        $stmt->bind_param("s", $correo);
    
        // Ejecutamos la declaración
        $stmt->execute();
    
        // Obtenemos el resultado
        $result = $stmt->get_result();
    
        // Obtenemos el usuario
        $user = $result->fetch_assoc();
    
        // Cerramos la declaración
        $stmt->close();
    
        return $user;
    }
    
    public static function getUserByEmailAndPassword($data)
    {
        $conn = Database::connect();
        $correo = $data['correo'];
        $contrasenia = $data['contrasenia'];
    
        // Usa una declaración preparada para prevenir la inyección SQL
        $sql = "SELECT * FROM usuarios WHERE correo = ? AND contrasenia = ?";
        $stmt = $conn->prepare($sql);
    
        // Vincula los parámetros
        $stmt->bind_param("ss", $correo, $contrasenia);
    
        // Ejecuta la declaración
        $stmt->execute();
    
        // Obtiene el resultado
        $result = $stmt->get_result();
    
        // Obtiene el usuario
        $user = $result->fetch_assoc();
    
        // Cierra la declaración
        $stmt->close();
    
        return $user;
    }

    public static function createUser($data)
    {
        $conn = Database::connect();
        $nombre = $data['nombre'];
        $correo = $data['correo'];
        $contrasenia = $data['contrasenia'];
        $sql = "INSERT INTO usuarios (nombre, correo, contrasenia) VALUES ('$nombre', '$correo', '$contrasenia')";
        $conn->query($sql);
    }

    public static function updateUser($id, $data)
    {
        $conn = Database::connect();
        $nombre = $data['nombre'];
        $correo = $data['correo'];
        $contrasenia = $data['contrasenia'];
        $sql = "UPDATE usuarios SET nombre = '$nombre', correo = '$correo', contrasenia = '$contrasenia' WHERE id = $id";
        $conn->query($sql);
    }

    public static function deleteUser($id)
    {
        $conn = Database::connect();
        $sql = "DELETE FROM usuarios WHERE id = $id";
        $conn->query($sql);
    }
}
